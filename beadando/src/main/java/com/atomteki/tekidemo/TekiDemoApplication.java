package com.atomteki.tekidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class TekiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TekiDemoApplication.class, args);
    }

}
