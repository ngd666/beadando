package com.atomteki.tekidemo.repository.impl;

import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.vaadin.flow.component.notification.Notification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class RoomRepositoryImpl implements RoomRepository {

    @PersistenceContext
    public EntityManager em;

    @Override
    public List<Room> findAll() throws Exception {
        return em.createNamedQuery(Room.FIND_ALL).getResultList();
    }

    @Override
    public void save(Room room) throws Exception {
        em.persist(room);
    }

    @Override
    public void delete(Long id) throws Exception {
        em.remove(findById(id));

    }

    @Override
    public void update(Room room) throws Exception {
        em.merge(room);
    }


    @Override
    public Room findById(Long id) throws Exception {
        return em.find(Room.class, id);
    }
}