package com.atomteki.tekidemo.util;

public interface RefreshAware {

    void processRefresh();
}