package com.atomteki.tekidemo.entity;

import javax.persistence.*;

@NamedQuery(name = Room.FIND_ALL, query = "select n from Room n")
@Table
@Entity
public class Room {
    public static final String FIND_ALL = "Room.findAll";

    @Id
    @GeneratedValue
    private Long    id;
    private String  name;
    private Integer level;
//--CONSTRUCTORS---------------------------------
    public Room() { }
//--GETTERS--------------------------------------
    public Long         getId       ()                  { return id; }
    public String       getName     ()                  { return name; }
    public Integer      getLevel    ()                  { return level; }
//--SETTERS--------------------------------------
    public void         setId       (Long id)           { this.id       = id; }
    public void         setName     (String name)       { this.name     = name; }
    public void         setLevel    (Integer level)     { this.level    = level; }
}