package com.atomteki.tekidemo.component;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class MenuBar extends HorizontalLayout {

    public MenuBar() {
        add(BuildButton("Kurzusok" ,   "/course"   ,VaadinIcon.CALC_BOOK));
        add(BuildButton("Főoldal",     "/"         ,VaadinIcon.HOME_O));
        add(BuildButton("Termek" ,     "/room"     ,VaadinIcon.BUILDING_O));
    }
    public MenuBar(HorizontalLayout parentLayout){
        parentLayout.add(BuildButton("Kurzusok" ,   "course"  ,VaadinIcon.CALC_BOOK));
        parentLayout.add(BuildButton("Főoldal",     ""        ,VaadinIcon.HOME_O));
        parentLayout.add(BuildButton("Termek" ,     "room"    ,VaadinIcon.BUILDING_O));
    }
    public MenuBar(VerticalLayout parentLayout){
        parentLayout.add(BuildButton("Kurzusok" ,   "course"  ,VaadinIcon.CALC_BOOK));
        parentLayout.add(BuildButton("Főoldal",     ""        ,VaadinIcon.HOME_O));
        parentLayout.add(BuildButton("Termek" ,     "room"    ,VaadinIcon.BUILDING_O));
    }

    private Button BuildButton(String text, String href, VaadinIcon icon) {
        Button button = new Button(text,icon.create());
        button.addClickListener(event-> {
            UI.getCurrent().navigate(href);
        });
        return button;
    }
}
