package com.atomteki.tekidemo.component;

import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.atomteki.tekidemo.util.RefreshAware;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringComponent
@UIScope
public class RoomForm extends VerticalLayout{

    private Room                room;

    @Autowired
    private RoomRepository      roomRepository;

    private Binder<Room>        binder;

    private RefreshAware        refreshAware;

    private TextField           name;
    private ComboBox<Integer>   level;

    Button viewPageButton;

    @PostConstruct
    private void init() {
        binder      = new Binder<>(Room.class);
        name        = new TextField("Terem neve");
        level       = new ComboBox<>();

        List<Integer> levels = new ArrayList<>() ;

        for(int i = -1;i <=10;i++)levels.add(i);

        level.setItems(levels);
        level.setLabel("Szint");

        add(VaadinIcon.BUILDING_O.create(),name,level);

        HorizontalLayout    buttonBar = new HorizontalLayout();

        Button saveButton     = new Button("Mentés",    VaadinIcon.PENCIL.create());
        Button deleteButton   = new Button("Törlés",    VaadinIcon.TRASH.create());
        Button cancelButton   = new Button("Mégsem",    VaadinIcon.CLOSE.create());
        viewPageButton        = new Button("Kapcsolat", VaadinIcon.LINK.create());

        buttonBar.add(saveButton,deleteButton,cancelButton,viewPageButton);
        add(buttonBar);

        saveButton.addClickListener(event -> {
            try {
                if (room.getId() == null) {
                    roomRepository.save(room);

                } else {
                    roomRepository.update(room);

                }
                setVisible(false);
                refreshAware.processRefresh();
                Notification.show("Sikerült elmenteni!!");
            } catch (Exception e) {
                Notification.show("NEM sikerült elmenteni!");
                e.printStackTrace();
            }
        });
        deleteButton.addClickListener(event -> {
            try {
                roomRepository.delete(room.getId());
                setVisible(false);
                refreshAware.processRefresh();
                Notification.show("Sikerült törölni az elemet!");
            } catch (Exception e) {
                Notification.show("NEM sikerült törölni az elemet!");
                e.printStackTrace();
            }
        });
        cancelButton.addClickListener(event -> {
            room = null;
            setVisible(false);
        });

        viewPageButton.addClickListener(e -> {
            UI.getCurrent().navigate("room/view/"+room.getId());
        });

        binder.bindInstanceFields(this);
        name.focus();
        setVisible(false);
    }
    public Anchor buildAnchor(String text, String href) {
        Anchor anchor = new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        return anchor;
    }

    public void initSave() {
        viewPageButton.setVisible(false);
        room = new Room();
        binder.setBean(room);
        setVisible(true);

    }

    public void initEdit(Long id) {

        try {
            viewPageButton.setVisible(true);
            this.room = roomRepository.findById(id);
            binder.setBean(room);
            setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setRefreshAware(RefreshAware refreshAware) {
        this.refreshAware = refreshAware;
    }
}
