package com.atomteki.tekidemo.component;

import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.entity.Course;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.atomteki.tekidemo.repository.CourseRepository;
import com.atomteki.tekidemo.util.RefreshAware;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class CourseForm extends VerticalLayout{

    private Course course;

    @Autowired
    private CourseRepository    courseRepository;
    @Autowired
    private RoomRepository      roomRepository;

    private Binder<Course>      binder;

    private TextField name;
    private ComboBox<Room> room;
    private RefreshAware refreshAware;

    Button viewPageButton;

    @PostConstruct
    private void init() {
        binder               = new Binder<>(Course.class);
        name = new TextField("Kurzus neve");
        room = new ComboBox<>();
        Text courseNameLabel = new Text("Kurzus neve");

        room.setLabel("Terem");
        try{
           room.setItems(roomRepository.findAll());
           room.setItemLabelGenerator(Room::getName);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        add(courseNameLabel, name, room);

        HorizontalLayout    buttonBar = new HorizontalLayout();

        Button saveButton     = new Button("Mentés", VaadinIcon.PENCIL.create());
        Button deleteButton   = new Button("Törlés", VaadinIcon.TRASH.create());
        Button cancelButton   = new Button("Mégsem", VaadinIcon.CLOSE.create());
        viewPageButton = new Button("Kapcsolat", VaadinIcon.LINK.create());

        buttonBar.add(saveButton,deleteButton,cancelButton,viewPageButton);
        add(buttonBar);

        saveButton.addClickListener(event -> {
            try {
                if (course.getId() == null) {
                    courseRepository.save(course);

                } else {
                    courseRepository.update(course);

                }
                setVisible(false);
                refreshAware.processRefresh();
                Notification.show("Sikerült elmenteni!!");
            } catch (Exception e) {
                Notification.show("NEM sikerült elmenteni!");
                e.printStackTrace();
            }
        });
        deleteButton.addClickListener(event -> {
            try {
                courseRepository.delete(course.getId());
                setVisible(false);
                refreshAware.processRefresh();
                Notification.show("Sikerült törölni az elemet!");
            } catch (Exception e) {
                Notification.show("NEM sikerült törölni az elemet!");
                e.printStackTrace();
            }
        });
        cancelButton.addClickListener(event -> {
            course = null;
            setVisible(false);
        });

        viewPageButton.addClickListener(e -> {
            UI.getCurrent().navigate("course/view/"+course.getId());
        });
        viewPageButton.setVisible(false);

        binder.bindInstanceFields(this);
        name.focus();
        setVisible(false);
    }
    public Anchor buildAnchor(String text, String href) {
        Anchor anchor = new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        return anchor;
    }

    public void initSave() {
        viewPageButton.setVisible(false);
        course = new Course();
        binder.setBean(course);
        setVisible(true);

    }

    public void initEdit(Long id) {

        try {
            viewPageButton.setVisible(true);
            this.course = courseRepository.findById(id);
            binder.setBean(course);
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setRefreshAware(RefreshAware refreshAware) {
        this.refreshAware = refreshAware;
    }
}
