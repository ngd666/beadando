package com.atomteki.tekidemo.view;

import com.atomteki.tekidemo.component.MenuBar;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.Route;

import javax.transaction.Transactional;

@Transactional
@Route
public class MainView extends AbstractView {
    MainView(){
        InitLayers();
        Label headerLabel = new Label("Főmenü");
        Label contentAreaLabel0 = new Label("Egy kis leírás:");
        Label contentAreaLabel1 = new Label
                ("Remélem fain lett az oldal, van egy keret amit minden oldalhoz beillesztettem, ez az Abstract View," +
                        "ami megadja a kinézetét, meg tömbösíti az oldalt. Próbáltam pár (asszem) css kódot belevinni, mint pl" +
                        "a címsor mérete meg a tömböknek a mérete a képernyőhöz képest." +
                        "A kurzusoknál nem igazán tudom hogy lehetne kiiratni a terem nevét a táblázatba, de biztos annak is meg van a módja." +
                        "minél hülyebiztosabbra szerettem volna csinálni, mikor rákattint a felhasználó a Kurzusok vagy a Termek oldalon a kis plusz" +
                        "gombra akkor nem jelenik meg a  van \"kapcsolat\" gomb ami megmutatja a kapcsolatot terem és kurzus között, de ha a teremre, " +
                        "vagy a kurzusra nyomunk rá akkor megjelenik fancy ikonnal :D . Illetve ha még nincs hozzáadva terem akkor a kurzusnál kiírja hogy " +
                        "vegyél fel termet hogy kurzust tudj létrehozni. Milyen ötletes mi? :D ");
        Label contenAreaLabel2 = new Label
                ("Remélem tetszik az oldal, rengeteg sok érdekes dolog van benne pl kurzusokat tudsz felvenni meg törölni... ja meg a termekről" +
                        "nem is beszélve... és kb ennyi.");



        headerHLayout.add(headerLabel);
        MenuBar menuBar = new MenuBar(menuVLayout);
        contentAreaVLayout.add(contentAreaLabel0,contentAreaLabel1,contenAreaLabel2);

        headerHLayout.setVerticalComponentAlignment(Alignment.CENTER,headerLabel);

    }
}
