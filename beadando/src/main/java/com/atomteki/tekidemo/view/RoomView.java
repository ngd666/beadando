package com.atomteki.tekidemo.view;

import com.atomteki.tekidemo.component.MenuBar;
import com.atomteki.tekidemo.component.RoomForm;
import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.atomteki.tekidemo.util.RefreshAware;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route
public class RoomView extends AbstractView implements RefreshAware {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomForm roomForm;
    private Grid<Room> grid;

    @PostConstruct
    private void init() {
        try {
            grid = new Grid<>();
            grid.setItems(roomRepository.findAll());
            grid.addColumn(Room::getId).setHeader("Id");
            grid.addColumn(Room::getName).setHeader("Terem neve");
            grid.addColumn(Room::getLevel).setHeader("Szint");
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (e.getValue() != null) {
                    roomForm.initEdit(e.getValue().getId());
                }
            });

            Button newButton = new Button(VaadinIcon.PLUS_CIRCLE.create());
            newButton.addClickListener( event ->{ roomForm.initSave(); });

            InitLayers();
            Label   headerLabel = new Label("Termek");
            MenuBar menuBar     = new MenuBar(menuVLayout);
            headerHLayout.add(headerLabel);



            contentAreaVLayout.add(newButton,grid,roomForm);

            roomForm.setRefreshAware(this);

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void processRefresh() {
        try{
            grid.setItems(roomRepository.findAll());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
