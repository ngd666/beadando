package com.atomteki.tekidemo.view;

import com.atomteki.tekidemo.component.CourseForm;
import com.atomteki.tekidemo.component.MenuBar;
import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.entity.Course;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.atomteki.tekidemo.repository.CourseRepository;
import com.atomteki.tekidemo.util.RefreshAware;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route
public class CourseView extends AbstractView implements RefreshAware {

    @Autowired
    private CourseRepository    courseRepository;
    @Autowired
    private RoomRepository      roomRepository;
    @Autowired
    private CourseForm          courseForm;
    private Grid<Course>        grid;

    @PostConstruct
    private void init() {
        try {

            InitLayers();
            MenuBar menuBar = new MenuBar(menuVLayout);
            Label headerLabel = new Label("Kurzusok");
            List<Room> rooms = null;

            headerHLayout.add(headerLabel);

        ///////////////////////////////////////////////////////////////////////////////
        ///ha nincs room hozzáadva akkor csak egy linket mutat ami átvezet a Room fülre
        ///////////////////////////////////////////////////////////////////////////////
        try {
            if (roomRepository.findAll().isEmpty()) {
                Notification.show("Nincsenek még hozzáadva Termek, kérem adjon az adatbázishoz párat", 20000, Notification.Position.MIDDLE);
                contentAreaVLayout.add(buildAnchor("Termek létrehozása", "/room"));
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

            grid = new Grid<>();
            Button newDataButton = new Button( VaadinIcon.PLUS_CIRCLE.create());
            try {
                grid.setItems(courseRepository.findAll());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            grid.addColumn(Course::getId).setHeader("Id");
            grid.addColumn(Course::getName).setHeader("Név");
            grid.addColumn(Course::getRoom).setHeader("Terem");
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (e.getValue() != null) {
                    courseForm.initEdit(e.getValue().getId());
                }
            });

            newDataButton.addClickListener(event -> {courseForm.initSave();});

            contentAreaVLayout.add(newDataButton,grid,courseForm);
            courseForm.setRefreshAware(this);

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public Anchor buildAnchor(String text, String href) {
        Anchor anchor = new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        return anchor;
    }

    @Override
    public void processRefresh() {
        try {
            grid.setItems(courseRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
