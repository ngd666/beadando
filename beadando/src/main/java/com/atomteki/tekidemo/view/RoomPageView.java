package com.atomteki.tekidemo.view;

import com.atomteki.tekidemo.component.MenuBar;
import com.atomteki.tekidemo.entity.Course;
import com.atomteki.tekidemo.entity.Room;
import com.atomteki.tekidemo.repository.CourseRepository;
import com.atomteki.tekidemo.repository.RoomRepository;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "room/view")
public class RoomPageView extends AbstractView implements HasUrlParameter<String> {

    @Autowired
    private RoomRepository repository;
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        try {

            InitLayers();
            MenuBar menuBar = new MenuBar(menuVLayout);
            Label headerLabel = new Label("Termek kapcsolata");
            headerHLayout.add(headerLabel);

            Room room = repository.findById(Long.parseLong(s));

            contentAreaVLayout.add(new Label("Terem : " + room.getName()));
            Grid<Course> grid=new Grid<>();
            grid.setItems(courseRepository.findAllByRoomId(room.getId()));
            grid.addColumn(Course::getId).setHeader("Id");
            grid.addColumn(Course::getName).setHeader("Kurzus neve");
            contentAreaVLayout.add(grid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

