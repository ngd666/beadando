package com.atomteki.tekidemo.view;

import com.atomteki.tekidemo.component.MenuBar;
import com.atomteki.tekidemo.entity.Course;
import com.atomteki.tekidemo.repository.CourseRepository;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "course/view")
public class CoursePageView extends AbstractView implements HasUrlParameter<String> {

    @Autowired
    private CourseRepository repository;

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        try {

            InitLayers();
            MenuBar menuBar = new MenuBar(menuVLayout);
            Label headerLabel = new Label("Kurzus");
            headerHLayout.add(headerLabel);

            Notification.show(beforeEvent+s);
            Course course = repository.findById(Long.parseLong(s));
            if(course.getRoom()!=null){
                contentAreaVLayout.add(new Label(course.getName() + " kurzushoz rendelt terem: " + course.getRoom().getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}