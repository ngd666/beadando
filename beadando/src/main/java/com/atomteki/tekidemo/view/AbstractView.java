package com.atomteki.tekidemo.view;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class AbstractView extends VerticalLayout {
        VerticalLayout      baseVLayout         = new VerticalLayout();
        HorizontalLayout    headerHLayout       = new HorizontalLayout();
        HorizontalLayout    contentHLayout      = new HorizontalLayout();
        VerticalLayout      menuVLayout         = new VerticalLayout();
        VerticalLayout      contentAreaVLayout  = new VerticalLayout();
        HorizontalLayout    footerHLayout       = new HorizontalLayout();

    public void InitLayers() {
        Label footerLabel = new Label("Ez az alja az oldalnak. Egy csomó fontos dolog van itt, mint pl.: elérhetőségek, meg ilyen marhaságok.");

        //----------------------------------------------------------------
        headerHLayout.setSizeFull();
        headerHLayout.setVerticalComponentAlignment(Alignment.CENTER);
        headerHLayout.getStyle().set("margin","10px");
        headerHLayout.getStyle().set("padding","0px");
        headerHLayout.getStyle().set("heigh","20px");
        headerHLayout.getStyle().set("font-size", "24px");
        headerHLayout.getStyle().set("font-weight","bold");
        headerHLayout.getStyle().set("background-color","#f1f1f1");

        contentHLayout.setSizeFull();
        contentHLayout.getStyle().set("margin","10px");
        contentHLayout.getStyle().set("padding","0px");
        contentHLayout.getStyle().set("margin","10px");

        menuVLayout.setSizeFull();
        menuVLayout.getStyle().set("margin","10px");
        menuVLayout.getStyle().set("padding","0px");
        menuVLayout.getStyle().set("width","20%");
        menuVLayout.getStyle().set("background-color","#f1f1f1");

        contentAreaVLayout.setSizeFull();
        contentAreaVLayout.getStyle().set("margin","10px");
        contentAreaVLayout.getStyle().set("padding","0px");
        contentAreaVLayout.getStyle().set("width","80%");
        contentAreaVLayout.getStyle().set("background-color","#f1f1f1");

        footerHLayout.setSizeFull();
        footerHLayout.getStyle().set("margin","0px");
        footerHLayout.getStyle().set("padding","0px");
        footerHLayout.getStyle().set("heigh","20px");
        footerHLayout.getStyle().set("background-color","#f1f1f1");
        footerHLayout.add(footerLabel);
        //----------------------------------------------------------------
        contentHLayout.add(
                menuVLayout,
                contentAreaVLayout
        );
        baseVLayout.add(
                headerHLayout,
                contentHLayout,
                footerHLayout
        );




        add(baseVLayout);
        //----------------------------------------------------------------
    }
}
